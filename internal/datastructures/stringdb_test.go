/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package datastructures

import (
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

func TestFileSystemStringDB(t *testing.T) {
	dir, err := ioutil.TempDir("", ".pkg-stringdb-test")
	if err != nil {
		panic(err)
	}
	db, err := NewFileSystemStringDB(dir)
	if err != nil {
		panic(err)
	}
	defer db.Close(true)

	testDatabase(t, db)
}

func TestMemoryStringDB(t *testing.T) {
	db, err := NewMemoryStringDB()
	if err != nil {
		panic(err)
	}
	defer db.Close(true)

	testDatabase(t, db)
}

func testDatabase(t *testing.T, db StringDB) {
	assert.Equal(t, 0, db.GetCount())
	assert.Equal(t, nil, db.Add("foo", "bar"))
	assertDbGet(t, db, "foo", "bar")
	assert.Equal(t, 1, db.GetCount())
	assertError(t, errors.MultipleEntryError{}, db.Add("foo", "bar2"))
	assertDbGet(t, db, "foo", "bar")
	assert.Equal(t, 1, db.GetCount())
	assertDbContains(t, db, "not", false)
	assertDbContains(t, db, "foo", true)
	_, err := db.Get("not")
	assertError(t, errors.EntryNotFoundError{}, err)
	assert.Equal(t, 1, db.GetCount())
	assert.Equal(t, nil, db.Add("value test", "content"))
	assert.Equal(t, 2, db.GetCount())
	assert.Equal(t, nil, db.Add("value test2", "content"))
	assert.Equal(t, 3, db.GetCount())
}

func assertDbGet(t *testing.T, db StringDB, key string, expected string) {
	result, err := db.Get(key)
	assert.Equal(t, nil, err)
	assert.Equal(t, expected, result)
}

func assertDbContains(t *testing.T, db StringDB, key string, expected bool) {
	result, err := db.ContainsKey(key)
	assert.Equal(t, nil, err)
	assert.Equal(t, expected, result)
}

func assertError(t *testing.T, expected error, actual error) bool {
	if fmt.Sprintf("%T", expected) != fmt.Sprintf("%T", actual) {
		assert.Fail(t, fmt.Sprintf("Errors not equal:\n  Expected: %T\n  Actual:   %T", expected, actual))
		return false
	}
	return true
}
