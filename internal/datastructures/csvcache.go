/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package datastructures

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

// CsvCache stores csv lines of equal length in an arbitrary StringDB implementation.
type CsvCache struct {
	headerCount int
	db          StringDB
}

// NewCsvCache returns a CsvCache for the given headers using a memory or file-system mapped StringDB implementation.
func NewCsvCache(headers []string, useFileSystem bool) (*CsvCache, error) {
	if useFileSystem {
		return NewFileSystemCsvCache(len(headers))
	}
	return NewMemoryCsvCache(len(headers))
}

// NewFileSystemCsvCache returns a CsvCache for the given headers using a file-system mapped StringDB implementation.
func NewFileSystemCsvCache(headerCount int) (*CsvCache, error) {
	dir, err := ioutil.TempDir("", "rde-client-csvcache")
	if err != nil {
		return nil, err
	}
	os.RemoveAll(dir)
	db, err := NewFileSystemStringDB(dir)
	if err != nil {
		return nil, err
	}

	return &CsvCache{headerCount: headerCount, db: db}, nil
}

// NewMemoryCsvCache returns a CsvCache for the given headers using a memory mapped StringDB implementation.
func NewMemoryCsvCache(headerCount int) (*CsvCache, error) {
	db, err := NewMemoryStringDB()
	if err != nil {
		return nil, err
	}

	return &CsvCache{headerCount: headerCount, db: db}, nil
}

// GetCount returns the total number of cached lines.
func (cache *CsvCache) GetCount() int {
	return cache.db.GetCount()
}

// Add adds an entry to the cache using the first array element as key. Returns an error if the key is already stored.
func (cache *CsvCache) Add(values []string) error {
	if len(values) != cache.headerCount {
		return errors.InvalidColumnCountError{Msg: fmt.Sprintf("Row with invalid number of values found near '%v'", values)}
	}

	key := values[0]

	// StringDB only supports string as value -> assemble single string that represents the entry separated by a 0-char
	var sb strings.Builder
	sb.WriteString(key)
	for i := 1; i < len(values); i++ {
		sb.WriteRune('\x00')
		sb.WriteString(values[i])
	}

	return cache.db.Add(key, sb.String())
}

// ContainsKey returns true when the given key is stored in the cache, otherwise false.
func (cache *CsvCache) ContainsKey(key string) (bool, error) {
	return cache.db.ContainsKey(key)
}

// Get returns all columns of a cached line. Returns an error when the key is not stored in the cache.
func (cache *CsvCache) Get(key string) ([]string, error) {
	result, err := cache.db.Get(key)
	if err != nil {
		return nil, err
	}

	parts := strings.Split(string(result), "\x00")
	if len(parts) != cache.headerCount {
		return nil, errors.InvalidDatabaseEntry{Msg: "Invalid entry in database"}
	}

	return parts, nil
}

// Close disposes all resources of the cache.
func (cache *CsvCache) Close() {
	cache.db.Close(true)
}
