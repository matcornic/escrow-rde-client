/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package deposits

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

// DepositInfo represents meta data of a deposit stored on the local file-system.
type DepositInfo struct {
	baseDir        string
	fullCsvFiles   []string
	incCsvFiles    []string
	hdlCsvFiles    []string
	depositFiles   []os.FileInfo
	domainHeaders  []string
	handleHeaders  []string
	requireHandles bool
}

// GetBaseDir returns the directory containing the deposit files.
func (di *DepositInfo) GetBaseDir() string {
	return di.baseDir
}

// GetFullCsvFiles returns all domain files of a 'full' deposit.
func (di *DepositInfo) GetFullCsvFiles() []string {
	return di.fullCsvFiles
}

// GetIncrementalCsvFiles returns all domain files of an 'inc' deposit.
func (di *DepositInfo) GetIncrementalCsvFiles() []string {
	return di.incCsvFiles
}

// GetDomainCsvFiles returns all domain files of a 'full' or 'inc' deposit.
func (di *DepositInfo) GetDomainCsvFiles() []string {
	if di.IsFullDeposit() {
		return di.fullCsvFiles
	} else if di.IsIncrementalDeposit() {
		return di.incCsvFiles
	}
	return nil
}

// GetHandleCsvFiles returns all handle ('hdl') files of a deposit.
func (di *DepositInfo) GetHandleCsvFiles() []string {
	return di.hdlCsvFiles
}

// GetDepositFiles returns all files of a deposit ('full', 'inc', and 'hdl').
func (di *DepositInfo) GetDepositFiles() []os.FileInfo {
	return di.depositFiles
}

// IsFullDeposit returns true when the deposit is a full deposit.
func (di *DepositInfo) IsFullDeposit() bool {
	return len(di.fullCsvFiles) > 0
}

// IsIncrementalDeposit returns true when the deposit is an incremental deposit.
func (di *DepositInfo) IsIncrementalDeposit() bool {
	return len(di.incCsvFiles) > 0
}

// HasDomainFiles returns true when at least one full or incremental deposit file is present.
func (di *DepositInfo) HasDomainFiles() bool {
	return (len(di.fullCsvFiles) + len(di.incCsvFiles)) > 0
}

// HasHandleFiles returns true when at least on handle file is present.
func (di *DepositInfo) HasHandleFiles() bool {
	return len(di.hdlCsvFiles) > 0
}

// GetDomainHeaders returns the csv headers defined in the first domain file.
func (di *DepositInfo) GetDomainHeaders() []string {
	return di.domainHeaders
}

// GetHandleHeaders returns the csv headers defined in the first handle file.
func (di *DepositInfo) GetHandleHeaders() []string {
	return di.handleHeaders
}

// FindDomainColumnIndex returns the column index in the domain headers containing the given string. Returns an error if no suitable column is found.
func (di *DepositInfo) FindDomainColumnIndex(containedString string) (int, error) {
	return findColumnIndex(di.domainHeaders, containedString)
}

// FindHandleColumnIndex returns the column index in the handle headers containing the given string. Returns an error if no suitable column is found.
func (di *DepositInfo) FindHandleColumnIndex(containedString string) (int, error) {
	return findColumnIndex(di.handleHeaders, containedString)
}
func findColumnIndex(arr []string, containedString string) (int, error) {
	for i, header := range arr {
		if strings.Contains(header, containedString) {
			return i, nil
		}
	}
	return 0, errors.ColumnNotFoundError{Msg: fmt.Sprintf("Header containing '%v' not found", containedString)}
}

// RequiresHandles returns true if at least one domain column requires handles.
func (di *DepositInfo) RequiresHandles() bool {
	return di.requireHandles
}

// NewDepositInfo reads all deposit meta data for the given directory. Use multi = true to enable splitted deposits.
func NewDepositInfo(baseDir string, multi bool) (*DepositInfo, error) {
	var di *DepositInfo
	var err error
	if multi {
		di, err = newMultiDepositInfo(baseDir)
	} else {
		di, err = newSingleDepositInfo(baseDir)
	}
	if err != nil {
		return nil, err
	}

	err = di.prepareMetaData()
	if err != nil {
		return nil, err
	}

	return di, nil
}

func newSingleDepositInfo(baseDir string) (*DepositInfo, error) {
	files, err := ioutil.ReadDir(baseDir)
	if err != nil {
		return nil, err
	}

	var fullCsvFile, incCsvFile, hdlCsvFile string
	var depositFiles []os.FileInfo
	for _, csvfile := range files {
		filename := baseDir + "/" + csvfile.Name()

		if strings.Contains(csvfile.Name(), "full") {
			if fullCsvFile != "" {
				return nil, errors.MultipleDepositFilesError{Msg: "Multiple full deposit files detected"}
			}
			if incCsvFile != "" {
				return nil, errors.FullAndIncrementalDepositError{Msg: "Both full and incremental deposit file detected"}
			}
			fullCsvFile = filename
			depositFiles = append(depositFiles, csvfile)
		}
		if strings.Contains(csvfile.Name(), "inc") {
			if incCsvFile != "" {
				return nil, errors.MultipleDepositFilesError{Msg: "Multiple incremental deposit files detected"}
			}
			if fullCsvFile != "" {
				return nil, errors.FullAndIncrementalDepositError{Msg: "Both full and incremental deposit file detected"}
			}
			incCsvFile = filename
			depositFiles = append(depositFiles, csvfile)
		}
		if strings.Contains(csvfile.Name(), "hdl") {
			if hdlCsvFile != "" {
				return nil, errors.MultipleDepositFilesError{Msg: "Multiple handle deposit files detected"}
			}
			hdlCsvFile = filename
			depositFiles = append(depositFiles, csvfile)
		}
	}

	//TODO pure handle deposits valid?
	if fullCsvFile == "" && incCsvFile == "" {
		return nil, errors.NeitherFullNorIncrementalDepositError{Msg: "Neither full nor incremental deposit file detected"}
	}

	return &DepositInfo{baseDir: baseDir, fullCsvFiles: packString(fullCsvFile), incCsvFiles: packString(incCsvFile), hdlCsvFiles: packString(hdlCsvFile), depositFiles: depositFiles}, nil
}

func packString(str string) []string {
	if str == "" {
		return []string{}
	}
	return []string{str}
}

func newMultiDepositInfo(baseDir string) (*DepositInfo, error) {
	// we need to find the file stating the domains and perhaps also the handles.
	files, err := ioutil.ReadDir(baseDir)
	if err != nil {
		return nil, err
	}

	var fullCsvFiles, incCsvFiles, hdlCsvFiles []string
	var depositFiles []os.FileInfo
	for _, csvfile := range files {
		filename := baseDir + "/" + csvfile.Name()

		if strings.Contains(csvfile.Name(), "full") {
			if len(incCsvFiles) > 0 {
				return nil, errors.FullAndIncrementalDepositError{Msg: "Both full and incremental deposit file detected"}
			}
			fullCsvFiles = append(fullCsvFiles, filename)
			depositFiles = append(depositFiles, csvfile)
		}
		if strings.Contains(csvfile.Name(), "inc") {
			if len(fullCsvFiles) > 0 {
				return nil, errors.FullAndIncrementalDepositError{Msg: "Both full and incremental deposit file detected"}
			}
			incCsvFiles = append(incCsvFiles, filename)
			depositFiles = append(depositFiles, csvfile)
		}
		if strings.Contains(csvfile.Name(), "hdl") {
			hdlCsvFiles = append(hdlCsvFiles, filename)
			depositFiles = append(depositFiles, csvfile)
		}
	}

	//TODO pure handle deposits valid?
	if len(fullCsvFiles) == 0 && len(incCsvFiles) == 0 {
		return nil, errors.NeitherFullNorIncrementalDepositError{Msg: "Neither full nor incremental deposit file detected"}
	}

	//TODO re-use multi-deposit loading and skip sequence checking in the single case (check for max 1 fire per type)
	if err := sortAndCheckSequence(fullCsvFiles); err != nil {
		return nil, err
	}
	if err := sortAndCheckSequence(incCsvFiles); err != nil {
		return nil, err
	}
	if err := sortAndCheckSequence(hdlCsvFiles); err != nil {
		return nil, err
	}

	return &DepositInfo{baseDir: baseDir, fullCsvFiles: fullCsvFiles, incCsvFiles: incCsvFiles, hdlCsvFiles: hdlCsvFiles, depositFiles: depositFiles}, nil
}

func sortAndCheckSequence(files []string) error {
	if len(files) < 2 {
		// sequences with 0 and 1 entries do not require any action
		return nil
	}

	exp, err := regexp.Compile("(?i)^.*_(\\d+)(?:\\.csv)?$")
	if err != nil {
		return err
	}

	for i := 0; i < len(files); i++ {
		// find file with sequence number (i+1) in the remainder of the files array (analogously to selection sort)
		swapIndex := -1
		for j := i; j < len(files); j++ {
			groups := exp.FindStringSubmatch(files[j])
			if groups == nil {
				return errors.MissingSequenceNumberError{Msg: "Multiple files for 'full', 'inc' or 'hdl' must be indexed by a sequence number like 'foobar_full_1.csv'"}
			}
			num, err := strconv.Atoi(groups[1])
			if err != nil {
				return err
			}

			if num < 1 {
				return errors.InvalidSequenceNumberError{Msg: "Deposit files must begin with sequence number 1"}
			}

			if num == (i + 1) {
				// found file with sequence number (i+1):
				swapIndex = j
				break
			} else if num < (i + 1) {
				return errors.ConflictingSequenceNumberError{Msg: "Multiple deposit files with sequence number " + strconv.Itoa(num) + " found"}
			}
		}

		if swapIndex == -1 {
			return errors.MissingSequenceFile{Msg: "Missing deposit file with sequence number " + strconv.Itoa(i+1)}
		}

		if swapIndex != i {
			// swap the elements. remainder of the list is sorted in succeeding iterations
			tmp := files[i]
			files[i] = files[swapIndex]
			files[swapIndex] = tmp
		}
	}
	return nil
}

func (di *DepositInfo) prepareMetaData() error {
	var err error

	if di.HasDomainFiles() {
		di.domainHeaders, err = readHeaders(di.GetDomainCsvFiles()[0])
		if err != nil {
			return err
		}
	}

	if di.HasHandleFiles() {
		di.handleHeaders, err = readHeaders(di.hdlCsvFiles[0])
		if err != nil {
			return err
		}
	}

	if di.HasDomainFiles() {
		di.requireHandles = containsSubtext(di.domainHeaders, "handle")
	}

	return nil
}

func readHeaders(csvFile string) ([]string, error) {
	file, err := os.Open(csvFile)
	if err != nil {
		return nil, err
	}

	reader := csv.NewReader(file)
	headers, err := reader.Read()
	if err == io.EOF {
		return nil, errors.EmptyDepositFileError{Msg: "Empty deposit file found"}
	} else if err != nil {
		return nil, err
	}

	return headers, nil
}

func containsSubtext(haystack []string, needle string) bool {
	for _, str := range haystack {
		if strings.Contains(str, needle) {
			return true
		}
	}
	return false
}
