/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package logging

import (
	"bytes"
	"log"
	"strings"
	"testing"
)

func TestWrite(t *testing.T) {
	logger := new(Logger)
	buffer := bytes.NewBuffer(nil)
	logger.Destination = buffer
	log.SetOutput(logger)

	_, err := logger.Write([]byte(string("test char sequence")))
	if err != nil {
		t.Error(err)
	}

	if !strings.Contains(buffer.String(), "test char sequence") {
		t.Error("Log buffer does not contain logged entry")
	}
}
