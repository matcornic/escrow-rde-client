/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package logging

import (
	"fmt"
	"io"
	"time"
)

// Logger is a helper struct to store the logfile handle
type Logger struct {
	Destination io.Writer
}

// Write implements the write function needed by any logger.
func (writer Logger) Write(data []byte) (int, error) {
	logline := writer.format(string(data))
	return writer.Destination.Write([]byte(logline))
}

func (writer Logger) format(line string) string {
	return fmt.Sprintf("%-23s %s", time.Now().Local().Format("2006-01-02 15:04:05.999"), line)
}
